module github.com/lisyaoran51/GinGormAPI

go 1.13

require (
	github.com/gin-gonic/gin v1.7.7
	github.com/rogpeppe/godef v1.1.2 // indirect
	github.com/tpkeeper/gin-dump v1.0.1
	golang.org/x/tools v0.1.10 // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v8 v8.18.2
	gopkg.in/go-playground/validator.v9 v9.31.0
)
